<?php
namespace Mini\Exceptions;

use UnexpectedValueException;
class ValidationException extends UnexpectedValueException implements MiniExceptionInterface
{
}
