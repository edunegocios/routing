<?php

namespace Mini\Exceptions;

use RuntimeException;

/**
 * ResponseAlreadySentException
 *
 * Exception used for when a response is attempted to be sent after its already been sent
 */
class ResponseAlreadySentException extends RuntimeException implements MiniExceptionInterface
{
}
